﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gun : MonoBehaviour 
{
	[SerializeField] private Material BulletMaterial; 
	[SerializeField] private float BulletScale = 0.5f; 
	[SerializeField] private float RechargeTime = 0.25f; 
	[Range( 1, 100 )]
	[SerializeField] private int BulletPoolSize = 10; 

	private GameObject [] mBulletPool;
	private List<GameObject> mActiveBullets;
	private List<GameObject> mAvailableBullets;
	private float mRecharging;

	public List<GameObject> ActiveBullets { get { return mActiveBullets; } }

	void Awake()
	{
		// Create the bullets, initialise the active and available lists, put all bullets in the available list
		mActiveBullets = new List<GameObject>();
		mAvailableBullets = new List<GameObject>();
		mBulletPool = new GameObject[BulletPoolSize];
		for( int count = 0; count < BulletPoolSize; count++ )
		{
			GameObject bullet = new GameObject( "Bullet_PoolID" + ( count + 1 ) );
			CreateMesh m = bullet.AddComponent<CreateMesh>();
			m.Material = BulletMaterial;
			bullet.transform.localScale = new Vector3( BulletScale, BulletScale, BulletScale );
			bullet.transform.parent = transform;
			mAvailableBullets.Add( bullet );
			bullet.SetActive( false );
		}
		mRecharging = 0.0f;
	}

	void Update()
	{
		// Update the position of each active bullet, keep a track of bullets which have gone off screen 
		List<GameObject> oldBullets = new List<GameObject>(); 
		for( int count = 0; count < mActiveBullets.Count; count++ )
		{
			Vector3 position = mActiveBullets[count].transform.position;
			position.y += Gameplay.GameDeltaTime * Gameplay.BulletSpeed;
			mActiveBullets[count].transform.position = position;
			if( position.y > Gameplay.ScreenHeight * 0.5f )
			{
				mActiveBullets[count].SetActive( false );
				oldBullets.Add( mActiveBullets[count] ); 
			}
		}

		// Remove the bullets which have gone off screen, return them to the available list
		for( int count = 0; count < oldBullets.Count; count++ )
		{
			mActiveBullets.Remove( oldBullets[count] );
			mAvailableBullets.Add( oldBullets[count] ); 
		}

		if( mRecharging > 0.0f )
		{
			mRecharging -= Gameplay.GameDeltaTime;
		}
	}

	public bool Fire( Vector3 position )
	{
		// Look for a free bullet and then fire it from the player position
		bool result = false;
		if( mAvailableBullets.Count > 0 && mRecharging <= 0.0f )
		{
			GameObject bullet = mAvailableBullets[0];
			bullet.transform.position = position;
			bullet.SetActive( true );
			mActiveBullets.Add( bullet );
			mAvailableBullets.Remove( bullet );
			mRecharging = RechargeTime;
			result = true;
		}

		// Returns true if a free bullet was found and fired
		return result;
	}
}
