InfiniteRunner
==============

Source assets for a simple Infinite Runner game made with Unity3D.

Created for Sheffield Hallam University, Tools Libraries and Frameworks module. 

Currently the source code for the Unity Project doesn't include any Inspector Extension Scripts. 

These will be added by students on the module.